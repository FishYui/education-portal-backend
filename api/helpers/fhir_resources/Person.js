class Person {

  constructor() {
    this.id = ''
    this.username = ''
    this.password = ''
    this.name = ''
    this.email = ''
    this.patient = ''
    this.practitioner = ''
  }

  set(input) {
    this.id = input.id
    this.username = input.username
    this.password = input.password
    this.name = input.name
    this.email = input.email
    this.patient = input.patient
    this.practitioner = input.practitioner
  }

  build() {

    let person = {
      'resourceType': 'Person',
      'id': '',
      'identifier': [],
      'name': [],
      'telecom': [],
      'link': []
    }

    if (this.id) {
      person.id = this.id;
    }

    if (this.username) {
      person.identifier.push({
        'system': 'username',
        'value': this.username
      });
    }

    if (this.password) {
      person.identifier.push({
        'system': 'password',
        'value': this.password
      });
    }

    if (this.name) {
      person.name.push({
        'text': this.name
      });
    }

    if (this.email) {
      person.telecom.push({
        'system': 'email',
        'value': this.email
      });
    }

    if (this.patient) {
      person.link.push({
        'target': {
          'reference': 'Patient/' + this.patient
        }
      });
    }

    if (this.practitioner) {
      person.link.push({
        'target': {
          'reference': 'Practitioner/' + this.practitioner
        }
      });
    }


    for (let i in person) {
      if (!person[i] || person[i] == '' || person[i].length == 0) {
        delete person[i];
      }
    }

    return person;
  }

  unbuild(obj) {

    //抓id
    if (obj.id) {
      this.id = obj.id;
    }

    //抓username、password
    if (obj.identifier) {
      for (let i of obj.identifier) {
        if (i.system === 'username') {
          this.username = i.value;
        } else if (i.system === 'password') {
          this.password = i.value;
        }
      }
    }

    //抓name
    if (obj.name && obj.name[0].text) {
      this.name = obj.name[0].text;
    }

    //抓email
    if (obj.telecom) {
      for (let t of obj.telecom) {
        if (t.system === 'email') {
          this.email = t.value;
          break;
        }
      }
    }

    //抓patient、practitioner
    if (obj.link) {
      for (let l of obj.link) {
        
        const reference = l.target.reference.split('/');
        
        if (reference[0] === 'Patient') {
          this.patient = reference[1];
        } else if (reference[0] === 'Practitioner') {
          this.practitioner = reference[1];
        }
      }
    }

  }
}

module.exports = {

  friendlyName: 'FHIR Person',

  description: 'FHIR Person Resource.',

  inputs: {

  },

  exits: {

  },

  fn: async function (inputs, exits) {

    const person = new Person();

    return exits.success(person);
  }

}