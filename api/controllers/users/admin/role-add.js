module.exports = {

  friendlyName: 'Add',

  description: 'Admin add User Role.',

  inputs: {

    personId: {
      required: false,
      type: 'string'
    },
    organization: {
      required: false,
      type: 'string'
    },
    role: {
      required: false,
      type: 'ref'
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const post_data = { personId, organization, role } = inputs;

    //檢查是否有輸入
    if (personId == undefined || role == undefined) {
      return exits.err('SIGNUP-0001');
    }

    //組織沒輸入就給預設(MITW)
    if (organization == undefined) {
      post_data.organization = sails.config.custom.original_organization.reference;
    }

    //取得用戶Practitioner
    const fhir_practitioner_response = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/Practitioner?_has:Person:practitioner:_id=${personId}`);
    if (!fhir_practitioner_response.success) {
      return exits.err('SERVER_ERROR');
    }
    if(fhir_practitioner_response.data.total === 0){
      return exits.err('ROLEADD-0001');
    }
    const original_practitioner = fhir_practitioner_response.data.entry[0].resource;

    //取得用戶PractitionerRole
    const fhir_practitionerrole_response = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/PractitionerRole?practitioner=${original_practitioner.id}&organization=${organization}`);
    if (!fhir_practitionerrole_response.success) {
      return exits.err('SERVER_ERROR');
    }

    
    //用戶沒有PractitionerRole就新增
    if(fhir_practitionerrole_response.data.total === 0) {
      
      //new新的 PractitionerRole
      const new_practitionerrole = await sails.helpers.fhirResources.practitionerrole();
      post_data.practitioner = original_practitioner.id;
      new_practitionerrole.set(post_data);
      
      //上傳FHIR Server
      const post_result = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/PractitionerRole`, {
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify(new_practitionerrole.build()) 
      });
      if (!post_result.success) {
        return exits.err('SERVER_ERROR');
      }

    } else if (fhir_practitionerrole_response.data.total === 1) {  //用戶有PractitionerRole就修改

      //取得原始 PractitionerRole
      const original_practitionerrole = fhir_practitionerrole_response.data.entry[0].resource;
      //new新的 PractitionerRole
      const new_practitionerrole = await sails.helpers.fhirResources.practitionerrole();

      //新的PractitionerRole先用原始的資料再修改權限編碼
      new_practitionerrole.unbuild(original_practitionerrole);
      new_practitionerrole.code = role;

      //上傳FHIR Server
      const put_result = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/PractitionerRole/${new_practitionerrole.id}`, {
        method: 'PUT',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify(Object.assign(original_practitionerrole, new_practitionerrole.build())) // 用新資料將舊資料替換掉
      });
      if (!put_result.success) {
        return exits.err('SERVER_ERROR');
      }

    }

    return exits.success();
  }

}