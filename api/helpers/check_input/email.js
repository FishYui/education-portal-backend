module.exports = {

  friendlyName: 'Check Email',

  description: 'Check Input Email.',

  inputs: {

    email: {
      required: true,
      type: 'string'
    }

  },

  exits: {

  },

  fn: async function (inputs, exits) {

    const regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    
    if (!regex.test(inputs.email)) {
      return exits.success({ success: false });
    }

    return exits.success({ success: true });
  }

}