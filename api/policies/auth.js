module.exports = async function (req, res, next) {

  const authorization = req.headers.authorization;

  if (!authorization) {
    return res.err('AUTH-0001');
  }

  const [authType, authToken] = authorization.split(' ');

  if (authType != 'Bearer' || !authToken) {
    return res.err('AUTH-0002');
  }

  const result = await sails.helpers.jwt.verifyJwt(authToken);

  if (!result.success) {
    return res.err(result.code);
  }

  const payload = result.payload;

  const session = await Sessions.findOne(payload.jti);
  if(!session) {
    return res.err('AUTH-0002');
  }

  
  req.thisUser = {
    jwtPayload: payload
  }

  return next();
}