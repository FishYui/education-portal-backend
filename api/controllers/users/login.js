module.exports = {

  friendlyName: 'LogIn',

  description: 'User LogIn.',

  inputs: {

    username: {
      required: false,
      type: 'string',
    },
    password: {
      required: false,
      type: 'string',
    },

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const { username, password } = inputs;

    if (username == undefined || password == undefined) {
      return exits.err('LOGIN-0001');
    }

    //依照username取得用戶(Person)
    const response = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/Person?identifier=username|${username}`);
    if (!response.success) {
      return exits.err('SERVER_ERROR');
    }

    const bundlePerson = response.data;

    //檢查帳號是否存在
    if (bundlePerson.total == 0) {
      return exits.err('LOGIN-0002');
    }
    //檢查帳號是否有多個
    if (bundlePerson.total > 1) {
      return exits.err('LOGIN-0003');
    }

    const personData = bundlePerson.entry[0].resource;

    //檢查密碼是否一致
    const fhirPassword = personData.identifier.filter(i => i.system == 'password');
    if (password != fhirPassword[0].value) {
      return exits.err('LOGIN-0004');
    }

    //核發Token
    const jwtpayload = {
      userId: personData.id
    }
    const jwtOption = {
      subject: 'Authentication Token'
    }
    const jwtToken = await sails.helpers.jwt.issueJwt(jwtpayload, jwtOption);
    if (!jwtToken.success) {
      return exits.err('SERVER_ERROR');
    }


    const data = {
      token: jwtToken.data,
      id: personData.id
    }

    return exits.success(data);
  }

}