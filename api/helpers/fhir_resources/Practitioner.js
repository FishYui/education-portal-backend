class Practitioner {

  constructor() {
    this.id = ''
  }

  set(input) {
    this.id = input.id
  }

  build() {

    const practitioner = {
      "resourceType": "Practitioner",
    }

    if (this.id) {
      practitioner.id = this.id;
    }


    for (let i in practitioner) {
      if (!practitioner[i] || practitioner[i] == '' || practitioner[i].length == 0) {
        delete practitioner[i];
      }
    }

    return practitioner;
  }

  unbuild(obj) {

    //抓id
    if (obj.id) {
      this.id = obj.id;
    }

  }
}

module.exports = {

  friendlyName: 'FHIR Practitioner',

  description: 'FHIR Practitioner Resource.',

  inputs: {

  },

  exits: {

  },

  fn: async function (inputs, exits) {

    const practitioner = new Practitioner();

    return exits.success(practitioner);
  }

}