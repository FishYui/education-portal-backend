const fetch = require('node-fetch');

module.exports = {

  friendlyName: 'Fetch Function',

  description: 'Fetch Function.',

  inputs: {

    url: {
      required: true,
      type: 'string'
    },
    option: {
      required: false,
      type: 'ref',
      defaultsTo: {}
    }

  },

  exits: {

  },

  fn: async function (inputs, exits) {
    
    try {

      const response = await fetch(inputs.url, inputs.option);

      if (!response.ok) {
        throw new Error(response.statusText);
      }

      const contentType = response.headers.get('Content-Type');

      if (contentType == null) {
        return exits.success({ success: true, data: await response.text() });
      }
      if (contentType.indexOf('json') > -1) {
        return exits.success({ success: true, data: await response.json() });
      }

    }
    catch (e) {
      return exits.success({ success: false, message: e.message });
    }
  }

}