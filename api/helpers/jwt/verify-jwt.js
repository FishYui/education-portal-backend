const jwt = require('jsonwebtoken');

module.exports = {


  friendlyName: 'Verify jwt',


  description: '',


  inputs: {

    token: {
      type: 'string',
      required: true
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    try {

      const payload = await jwt.verify(inputs.token, sails.config.custom.jwt.secret)

      return exits.success({ success: true, payload });
    } catch (err) {

      let code;

      if (err.name === 'TokenExpiredError') {
        code = 'AUTH-0003';
      } else if (err.name === 'JsonWebTokenError') {
        code = 'AUTH-0005';
      } else if (err.name === 'NotBeforeError') {
        code = 'AUTH-0006';
      } else {
        code = 'Other Err';
      }

      return exits.success({ success: false, code });
    }
  }
};
