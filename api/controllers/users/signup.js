module.exports = {

  friendlyName: 'SignUp',

  description: 'User SignUp.',

  inputs: {

    password: {
      required: false,
      type: 'string',
    },
    name: {
      required: false,
      type: 'string',
    },
    email: {
      required: false,
      type: 'string',
    },

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const post_data = { password, name, email } = inputs;
    post_data.username = post_data.email;

    //檢查是否有輸入
    if (password == undefined || name == undefined || email == undefined) {
      return exits.err('SIGNUP-0001');
    }

    //檢查Email格式
    const checkEmail = await sails.helpers.checkInput.email(email);
    if (!checkEmail.success) {
      return exits.err('SIGNUP-0002');
    }

    //檢查帳號是否已存在
    const checkUser = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/Person?identifier=username|${email}`);
    if (checkUser.data.total != 0) {
      return exits.err('SIGNUP-0003');
    }

    //新增學生角色
    const new_patient = await sails.helpers.fhirResources.patient();
    new_patient.set({ organization: sails.config.custom.original_organization });

    //新增職員角色
    const new_practitioner = await sails.helpers.fhirResources.practitioner();
    new_practitioner.set({});

    //用FHIR Bundle串接學生和職員
    const post_bundle_data = {
      "resourceType": "Bundle",
      "type": "transaction",
      "entry": [
        {
          "resource": new_patient.build(),
          "request": {
            "method": "POST",
            "url": "Patient"
          }
        },
        {
          "resource": new_practitioner.build(),
          "request": {
            "method": "POST",
            "url": "Practitioner"
          }
        }
      ]
    }

    //上傳FHIR Server
    const post_bundle_result = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(post_bundle_data)
    });
    if (!post_bundle_result.success) {
      return exits.err('SERVER_ERROR');
    }

    //將學生和職員角色給予用戶
    let tmp;
    tmp = post_bundle_result.data.entry[0].response.location.split('/');
    post_data.patient = tmp[1];
    tmp = post_bundle_result.data.entry[1].response.location.split('/');
    post_data.practitioner = tmp[1];

    //新增用戶
    const new_person = await sails.helpers.fhirResources.person();
    new_person.set(post_data);

    //上傳FHIR Server
    const post_person_result = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/Person`, {
      method: 'POST',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(new_person.build())
    });
    if (!post_person_result.success) {
      return exits.err('SERVER_ERROR');
    }

    return exits.success();
  }

}