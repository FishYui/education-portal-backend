module.exports = function ok(data) {

    const req = this.req;
    const res = this.res;

    return res.status(200).json({
        success: true,
        data
    });

}
