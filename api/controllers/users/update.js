module.exports = {

  friendlyName: 'Update',

  description: 'User Self Update.',

  inputs: {

    password: {
      required: false,
      type: 'string',
    },
    name: {
      required: false,
      type: 'string',
    },
    email: {
      required: false,
      type: 'string',
    }

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const req = this.req;
    const { password, name, email } = inputs;

    // 檢查是否有輸入
    if (password == undefined || name == undefined || email == undefined) {
      return exits.err('UPDATE-0001');
    }

    // 檢查Email格式
    const check_email = await sails.helpers.checkInput.email(email);
    if (!check_email.success) {
      return exits.err('UPDATE-0002');
    }

    // 取得用戶原始資料(Person + Patient)
    const fhir_response = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/Person?_id=${req.thisUser.jwtPayload.userId}&_include=Person:patient`);
    if (!fhir_response.success) {
      return exits.err('SERVER_ERROR');
    }
    const original_person = fhir_response.data.entry[0].resource;
    const original_patient = fhir_response.data.entry[1].resource;

    const new_person = await sails.helpers.fhirResources.person();
    // 依照原始資料重新創建新的資料
    new_person.unbuild(original_person)
    // 修改資料
    new_person.password = password;
    new_person.name = name;
    new_person.email = email;

    // 上傳FHIR Server
    const put_result = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/Person/${req.thisUser.jwtPayload.userId}`, {
      method: 'PUT',
      headers: {
        'content-type': 'application/json'
      },
      body: JSON.stringify(Object.assign(original_person, new_person.build())) // 用新資料將舊資料替換掉
    });
    if (!put_result.success) {
      return exits.err('SERVER_ERROR');
    }

    return exits.success(put_result.data);
  }

}