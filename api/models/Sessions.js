module.exports = {

  attributes: {

    id: {
      type: 'string',
      required: true
    },
    user: {
      type: 'number',
      required: true
    },
    expiredAt: {
      type: 'ref',
      columnType: 'bigInt'
    }

  },

  beforeCreate: function (data, proceed) {

    const datetime = Date.now();
    data.expiredAt = datetime + (sails.config.custom.jwt.exp * 1000);

    return proceed();
  },

  customToJSON: function () {

    this.expiredAt = parseInt(this.expiredAt);

    return this;
  },

  updateTime: async function (sessionId) {

    const datetime = Date.now();
    expiredAt = datetime + (sails.config.custom.jwt.exp * 1000);

    await Sessions.updateOne({
      where: { id: sessionId }
    })
      .set({ expiredAt: expiredAt })
      .intercept(() => {
        return exits.err('Other Err');
      });
  }
  
}