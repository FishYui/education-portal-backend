module.exports.errcode = {

  code: {

    'AUTH-0001': {
      status: 400,
      message: '未加上 Authorization 標頭。'
    },
    'AUTH-0002': {
      status: 400,
      message: 'Authorization Token Undefined。'
    },
    'AUTH-0003': {
      status: 400,
      message: 'Authorization Token 過期。'
    },
    'AUTH-0004': {
      status: 400,
      message: '您所屬的身分組不允許執行此動作。'
    },
    'AUTH-0005': {
      status: 400,
      message: 'Authorization Token Error。'
    },
    'AUTH-0006': {
      status: 400,
      message: 'Authorization Token 尚未激活。'
    },

    'SIGNUP-0001': {
      status: 400,
      message: '資料輸入有遺漏'
    },
    'SIGNUP-0002': {
      status: 400,
      message: 'Email格式錯誤'
    },
    'SIGNUP-0003': {
      status: 400,
      message: '帳號已存在'
    },

    'LOGIN-0001': {
      status: 400,
      message: '資料輸入有遺漏'
    },
    'LOGIN-0002': {
      status: 400,
      message: '查無此帳號'
    },
    'LOGIN-0003': {
      status: 400,
      message: '有複數個相同帳號，請與管理員聯繫'
    },
    'LOGIN-0004': {
      status: 400,
      message: '密碼錯誤'
    },

    'UPDATE-0001': {
      status: 400,
      message: '資料輸入有遺漏'
    },
    'UPDATE-0002': {
      status: 400,
      message: 'Email格式錯誤'
    },

    'ROLEADD-0001': {
      status: 400,
      message: '查無此帳號'
    },

    'SERVER_ERROR' : {
      status: 500,
      message: '伺服器錯誤'
    },

  }

}