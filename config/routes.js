/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  'POST /user/signup': 'users/signup',  //用戶註冊
  'POST /user/login': 'users/login',  //用戶登入
  'POST /user/logout': 'users/logout',  //用戶登出
  'GET /user': 'users/detail',  //用戶登出
  'DELETE /user': 'users/delete',  //用戶刪除帳號
  'PUT /user': 'users/update',  //用戶修改帳號

  'POST /user/role/:personId': 'users/admin/role-add',  //用戶修改帳號
};
