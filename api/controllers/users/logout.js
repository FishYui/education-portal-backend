module.exports = {

  friendlyName: 'LogOut',

  description: 'User LogOut.',

  inputs: {

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const req = this.req;

    try {

      const deleteSession = await Sessions.destroyOne({ where: { id: req.thisUser.jwtPayload.jti } });

      return exits.success();
    } catch (e) {
      return exits.err('SERVER_ERROR');
    }
  }

}