module.exports = {

  friendlyName: 'Detail',

  description: 'User Self Detail.',

  inputs: {

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    const req = this.req;

    const response = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/Person?_id=${req.thisUser.jwtPayload.userId}&_include=Person:patient`);
    if (!response.success) {
      return exits.err('SERVER_ERROR');
    }

    const person = response.data.entry[0].resource;
    const patient = response.data.entry[1].resource;


    const data = {
      name: person.name[0].text,
      email: person.telecom[0].value,
      organization: {
        reference: patient.managingOrganization.reference,
        display: patient.managingOrganization.display
      }
    }

    return exits.success(data);
  }

}