class PractitionerRole {

  constructor() {
    this.id = ''
    this.user = ''
    this.practitioner = ''
    this.organization = ''
    this.code = []
  }

  set(input) {
    this.id = input.id
    this.user = input.personId
    this.practitioner = input.practitioner
    this.organization = input.organization
    this.code = input.role
  }

  build() {

    const practitionerRole = {
      'resourceType': 'PractitionerRole',
      'id': '',
      'identifier': [],
      'practitioner': {},
      'organization': {},
      'code': [
        {
          'coding': []
        }
      ]
    }

    practitionerRole.id = this.id;

    if (this.user) {
      practitionerRole.identifier.push({ system: 'user', value: this.user })
    }

    if (this.practitioner) {
      practitionerRole.practitioner = {
        'reference': 'Practitioner/' + this.practitioner
      }
    }

    if (this.organization) {
      practitionerRole.organization = {
        'reference': 'Organization/' + this.organization
      }
    }

    for (let c of this.code) {
      practitionerRole.code[0].coding.push({ system: sails.config.custom.codeSystem.role, code: c })
    }


    for (let i in practitionerRole) {
      if (!practitionerRole[i] || practitionerRole[i] == '' || practitionerRole[i].length == 0) {
        delete practitionerRole[i];
      }
    }

    return practitionerRole;
  }

  unbuild(obj) {

    if (obj.id) {
      this.id = obj.id;
    }

    for (let i of obj.identifier) {
      if (i.system === 'user') {
        this.user = i.value;
        break;
      }
    }

    if (obj.practitioner) {
      this.practitioner = obj.practitioner.reference.split('/')[1];
    }

    if (obj.organization) {
      this.organization = obj.organization.reference.split('/')[1];
    }

    if (obj.code) {
      for (let c of obj.code[0].coding) {
        this.code.push(c.value);
      }
    }

  }
}

module.exports = {

  friendlyName: 'FHIR PractitionerRole',

  description: 'FHIR PractitionerRole Resource.',

  inputs: {

  },

  exits: {

  },

  fn: async function (inputs, exits) {

    const practitionerRole = new PractitionerRole();

    return exits.success(practitionerRole);
  }

}