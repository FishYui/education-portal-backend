const jwt = require('jsonwebtoken');
const uuid = require('uuid/v4');

module.exports = {


  friendlyName: 'Issue jwt',


  description: '',


  inputs: {
    payload: {
      type: 'ref',
      defaultsTo: {}
    },
    option: {
      type: 'ref',
      defaultsTo: {}
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    try {

      const { payload, option } = inputs;

      const payloads = Object.assign({
        iat: Math.floor(Date.now() / 1000),
      }, payload);

      const tokenId = uuid();

      const options = Object.assign({
        issuer: sails.config.custom.jwt.iss,
        expiresIn: sails.config.custom.jwt.exp,
        jwtid: tokenId,
      }, option);

      const token = await jwt.sign(payloads, sails.config.custom.jwt.secret, options);

      await Sessions.create({ id: tokenId, user: payload.userId });

      return exits.success({ success: true, data: token });
    }
    catch (e) {
      return exits.success({ success: false });
    }
  }

};
