class Patient {

  constructor() {
    this.id = ''
    this.organization = {}
  }

  set(input) {
    this.id = input.id
    this.organization = input.organization
  }

  build() {

    const patient = {
      "resourceType": "Patient",
      "managingOrganization": {
        
      }
    }

    if (this.id) {
      patient.id = this.id;
    }

    if (this.organization) {
      patient.managingOrganization = {
        'reference': 'Organization/' + this.organization.reference,
        'display': this.organization.display
      }
    }


    for (let i in patient) {
      if (!patient[i] || patient[i] == '' || patient[i].length == 0) {
        delete patient[i];
      }
    }

    return patient;
  }

  unbuild(obj) {

    //抓id
    if (obj.id) {
      this.id = obj.id;
    }

    //抓管理組織
    if(obj.managingOrganization) {
      this.organization.reference = obj.managingOrganization.reference.split('/')[1];
      this.organization.display = obj.managingOrganization.display;
    }

  }
}

module.exports = {

  friendlyName: 'FHIR Patient',

  description: 'FHIR Patient Resource.',

  inputs: {

  },

  exits: {

  },

  fn: async function (inputs, exits) {

    const patient = new Patient();

    return exits.success(patient);
  }

}