module.exports = {

  friendlyName: 'Delete',

  description: 'User Self Delete.',

  inputs: {

  },

  exits: {

    success: {
      responseType: 'ok'
    },
    err: {
      responseType: 'err'
    }

  },

  fn: async function (inputs, exits) {

    try {

      const req = this.req;

      //刪除用戶的Sessions
      await Sessions.destroy({ where: { user: req.thisUser.jwtPayload.userId } });

      //刪除用戶帳號
      const deletePerson = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/Person/${req.thisUser.jwtPayload.userId}`, {
        method: 'DELETE'
      });
      if (!deletePerson.success) {
        throw new Error();
      }

      return exits.success();
    } catch (e) {
      return exits.err('SERVER_ERROR');
    }
  }

}


    // const responsePerson = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/Person/${req.thisUser.jwtPayload.userId}?_elements=link`);

    // const link1 = responsePerson.data.link[0].target.reference;
    // const link2 = responsePerson.data.link[1].target.reference;

    // const patient = link1.split('/')[0] === 'Patient' ? link1 : link2;
    // const practitioner = link2.split('/')[0] === 'Practitioner' ? link2 : link1;

    // const responsePractitionerRole = await sails.helpers.fetch(`${sails.config.custom.fhirServerUrl}/PractitionerRole?practitioner=${practitioner}&__summary=text`);